CREATE OR REPLACE PACKAGE xxtest_har_pkg AS

/* $XX_Header: xxtest_har_pkg.pks 11.5.10.1 2019/08/15 Harshavardhan_M $*/
/* $XX_Release: Standalone: RFG0001 $*/
/* CC_REF_NEW */
/* CC_REF_OLD */
--+=====================================================================+
--|                       R**** F***** G****                            |
--|                       P****** S****                                 |
--+=====================================================================+
--|                                                                     |
--|                                                                     |
--| TYPE          : Package Body                                        |
--| NAME          : xxtest_har_pkg                                      |
--| PURPOSE       : Created for test purpose                            |
--|                                                                     |
--| Modification History:                                               |
--| ---------------------                                               |
--|                                                                     |
--| Author           Date        Version         Description            |
--| -----------    -----------  ------------   ----------------         |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision        |
 --+====================================================================+
 
--+=======================================================================+
--| TYPE             : Procedure                                          |
--| NAME             : xxtest_har_output                                  |
--| INPUT Parameters : p_opco                                             |
--| OUTPUT Parameters: x_errbuff                                          |
--|                   ,x_retcode                                          |
--| PURPOSE          : Created for test purpose                           |
--| Modification History:                                                 |
--|----------------------                                                 |
--|                                                                       |
--| Author            Date        Version         Description             |
--| -----------     -----------  ------------     ----------------        |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision          |
--+=======================================================================+

  PROCEDURE xxtest_har_output(
             x_errbuff OUT NOCOPY VARCHAR2
            ,x_retcode OUT NOCOPY NUMBER
            ,p_opco hr_operating_units.organization_id%TYPE
            );
END xxtest_har_pkg;
/