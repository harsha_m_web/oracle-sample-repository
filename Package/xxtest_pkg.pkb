CREATE OR REPLACE PACKAGE BODY xxtest_har_pkg AS
/* $XX_Header: xxtest_har_pkg.pkb 11.5.10.4 2019/09/18 Harshavardhan_M $*/
/* $XX_Release: Standalone: RFG0005 $*/
/* CC_REF_NEW */
/* CC_REF_OLD */
--+=====================================================================+
--|                       R**** F***** G****                            |
--|                       P****** S****                                 |
--+=====================================================================+
--|                                                                     |
--| TYPE          : Package Body                                        |
--| NAME          : xxtest_har_pkg                                      |
--| PURPOSE       : Created for test purpose                            |
--|                                                                     |
--| Modification History:                                               |
--| ---------------------                                               |
--|                                                                     |
--| Author           Date        Version         Description            |
--| -----------    -----------  ------------   ----------------         |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision        |
--| (Harsha)        09-SEP-2019  11.5.10.2      RFG0002 - Updated       |
--| (Harsha)        09-SEP-2019  11.5.10.3      RFG0003 - Updated       |
--| (Harsha)        18-SEP-2019  11.5.10.4      RFG0005 - Updated       |
 --+====================================================================+
--+=======================================================================+
--| TYPE             : Procedure                                          |
--| NAME             : xxtest_har_output                                  |
--| INPUT Parameters : p_opco                                             |
--| OUTPUT Parameters: x_errbuff                                          |
--|                   ,x_retcode                                          |
--| PURPOSE          : Created for test purpose                           |
--| Modification History:                                                 |
--|----------------------                                                 |
--|                                                                       |
--| Author            Date        Version         Description             |
--| -----------     -----------  ------------     ----------------        |
--| (Harsha)        15-AUG-2019  11.5.10.1      Initial revision          |
--| (Harsha)        09-SEP-2019  11.5.10.2      RFG0002 - Updated         |
--| (Harsha)        09-SEP-2019  11.5.10.3      RFG0003 - Updated         |
--| (Harsha)        18-SEP-2019  11.5.10.4      RFG0005 - Updated         |
--+=======================================================================+
  PROCEDURE xxtest_har_output(
             x_errbuff OUT NOCOPY VARCHAR2
            ,x_retcode OUT NOCOPY NUMBER
            ,p_opco hr_operating_units.organization_id%TYPE
            )
  IS 
  
    CURSOR l_opco_name_cur(
             cp_opco hr_operating_units.organization_id%TYPE
             )
    IS
      SELECT hou.name
        FROM hr_operating_units hou
       WHERE hou.organization_id = cp_opco;
  
    g_opco_name       hr_operating_units.name%TYPE;
   
BEGIN
  
  OPEN l_opco_name_cur(
         cp_opco => p_opco
        );
  FETCH l_opco_name_cur
  INTO g_opco_name;
  CLOSE l_opco_name_cur;
  -- Ver No: 11.5.10.2 is added for RFG0002
  -- Ver No: 11.5.10.3 is added for RFG0003
  -- Ver No: 11.5.10.4 is added for RFG0005
  fnd_file.put_line ( fnd_file.log, g_opco_name );  
  
END xxtest_har_output;
END xxtest_har_pkg;
/